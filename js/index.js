$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e){
      console.log('el modal contacto se está mostrando');

      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-default');
      $('#contactoBtn').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function (e){
      console.log('el modal contacto se ha mostrado');
    });

    $('#contacto').on('hide.bs.modal', function (e){
      console.log('el modal contacto se está ocultando');
    });

    $('#contacto').on('hidden.bs.modal', function (e){
      console.log('el modal contacto se ha ocultado');
      $('#contactoBtn').prop('disabled', false);
      $('#contactoBtn').addClass('btn-outline-success');
    });
  });